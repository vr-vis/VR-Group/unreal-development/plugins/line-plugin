# Unreal Line Rendering Plugin

This plugin adds functionality to Unreal which allows a highly performant rendering of static and dynamic (poly-)lines. 

## Requirements

This plugin requires Unreal Engine 4.25.3. Unreal 4.25 added a (working) implementation of custom float data for instanced static meshes, and 4.25.3 fixed a few bugs with instance removal that are required.
No other dependecies are needed, the plugin even works without the Unreal Project Template. 

## Installation
The standard Unreal plugin installation method can be used. Just clone the plugin into the "Plugins" folder in your Unreal Project. 
Make sure to re-build the VS Project files, and add "InstancedMeshLineRendering" to the dependecy modules of your Project.Build.cs file to actually enable the plugin.

## Usage

All functionality is contained in a single component, [UGPUInstancedLineComponent](https://devhub.vr.rwth-aachen.de/VR-Group/unreal-development/line-plugin/-/blob/develop/Source/InstancedMeshLineRendering/Public/GPUInstancedLineComponent.h). 
The component can be added to any Actor, but it's recommended to just keep it in its own Actor. The local transform of neither Component nor Actor matter, as the lines will be moved to the correct
position in the vertex shader offset. 

The line rendering also works in the Editor viewport itself, and the Details panel of the Component can be used to add, remove and manipulate lines directly in the Editor viewport.
Those lines will persist in the actual Game itself as well. 

For direct manipulation, both C++ and Blueprint functions are available to dynamically add, update and remove lines and individual line segments. The plugin keeps an internal state of
the current rendered lines, which can be accessed via the line Id that is returned on adding a line. 

### Performance considerations

**Adding many lines at once:** 
Memory can be reserved by `ReserveMemory`, and the required number of lines can be initialized via `InitializeLinesInBulk`. This might introduce a slight lag, 
as the registration of many instances on the ISM unfortunatley cannot be done in bulk. Adding lines is ALWAYS a lot more expensive than updating lines!

**Render static lines:**
The rendering of static lines doesn't cost much performance, and millions of lines should be easily renderable above 90 fps. The camera-alignment is shader based and works for static lines as well. 
Updating a line or point, or even many, doesn't impact performance much as well.

**Render dynamic lines:**
Rendering lines that update each frame is more costly. Keeping the internal state consistent requires a least one memcpy (plus the texture update), 
which, for a million+ lines, can take a few milliseconds. If the application only needs to update all lines occasionally, and a consistent internal state for individual point updates and removals is needed, 
the function `UpdateLinesDataDirectly` can be used for a reasonably fast full update. 

If the application updates all lines each frame anyway, no internal state needs to be kept, and the simple, fast function `DrawLinesDirectly` should be used to render your line data in the fastest way.
No memcpy is executed to keep the internal state consistent, and the line data is directly copied into the texture memory. Both functions require the input data to be correctly arranged in a linear array (See code documentation for further info).