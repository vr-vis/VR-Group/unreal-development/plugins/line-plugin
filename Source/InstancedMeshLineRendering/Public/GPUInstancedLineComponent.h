// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "GPUInstancedLineComponent.generated.h"


USTRUCT(BlueprintType)
struct FEditorPoint
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, meta = (MakeEditWidget = true))
	FVector Point;

	FEditorPoint()
	{
		Point = FVector(0, 0, 0);
	}

	FEditorPoint(const FVector& InPoint)
		: Point(InPoint)
	{
	}

};

USTRUCT(BlueprintType)
struct FEditorLineData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, meta = (MakeEditWidget = true, EditFixedOrder))
	TArray<FEditorPoint> Points;

	UPROPERTY(EditAnywhere)
	float Width = 2.0;

	UPROPERTY(EditAnywhere)
	FColor Color;

	UPROPERTY(VisibleAnywhere)
	int32 RespectiveLineId = -1;
	
	FEditorLineData()
	{
		Points.SetNumZeroed(2);
		Width = 2.0f;
		Color = FColor::Red;
	}

	FEditorLineData(const TArray<FVector>& InPoints, float InWidth, const FColor& InColor)
		: Points(InPoints), Width(InWidth), Color(InColor)
	{
		if(Points.Num() < 2)
		{
			Points.SetNumZeroed(2);
		}
	}
};

USTRUCT()
struct FGPULineIndices
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	int32 InstanceIndex;

	UPROPERTY()
	int32 TextureIndex;

	FGPULineIndices() = default;

	FGPULineIndices(int32 InstanceIndex, int32 TextureIndex)
		: InstanceIndex(InstanceIndex), TextureIndex(TextureIndex) {}
};

USTRUCT()
struct FGPULineArray
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	TArray<FGPULineIndices> IndexArray;
};

UCLASS(Blueprintable, BlueprintType, meta = (BlueprintSpawnableComponent), HideCategories=(Object, LOD, Physics, Activation, Materials, "Cooking", "Sockets", "Collision", "Mobile", "HLOD"))
class INSTANCEDMESHLINERENDERING_API UGPUInstancedLineComponent : public UInstancedStaticMeshComponent
{
	GENERATED_BODY()

public:

	UGPUInstancedLineComponent(const FObjectInitializer& ObjectInitializer);
	virtual ~UGPUInstancedLineComponent();

	virtual void BeginPlay() override;

#if WITH_EDITOR
	virtual void PostEditChangeChainProperty(FPropertyChangedChainEvent& PropertyChangedEvent) override;
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override; // need this for the widget as for some godforsaken reason the Chain event doesn't fire...

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
#endif

	virtual void BeginDestroy() override;
	
private:

	void MoveTextureMarker()
	{
		CurrentTextureMarker.X++;
		if (CurrentTextureMarker.X == TextureWidth)
		{
			// move marker down one row:
			CurrentTextureMarker.X = 0;
			CurrentTextureMarker.Y = CurrentTextureMarker.Y + 1;
		}
	}


	void Init();

	void UpdateAllEditorLines();

	FUpdateTextureRegion2D* CalculateTextureRegions(const FIntPoint& StartIndex, int32 NumberOfPoints, int32& NumberOfRegionsOut);

	void ReleaseData(); // todo
	
	int32 AddNewSegmentInstance(const FLinearColor& Color, float Width, int32 Index);

	void InitializeCreatedSegmentInstance(const int InstanceId, const FLinearColor& Color, float Width, int32 Index);

	void UpdateTexture(const FIntPoint& StartIndex, int32 NumberOfPoints, uint8* SrcData, bool bMarkRenderStateDirty = true);

	void AddBulkInternal(int32 NumberOfLines, int32 NumberOfSegmentsPerLine, const TArray<FLinearColor>& Colors, const TArray<float>& Widths);
	
public:
	void UpdateWholeTexture() const;

	FLinearColor GetLineColor(int32 LineId);
	
	/**
	 * Reserves internal memory for a given amount of Lines and Segments per Line.
	 */
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	void ReserveMemory(int32 NumberOfSegments, int32 NumberOfLines);


	void ReserveMemoryWithoutSegments(int32 NumberOfLines, int32 NumberOfTotalPoints);

	
	/**
	 * Manually resized the texture to the specified width and height. Returns false if the texture couldn't be resized, e.g. if more lines are
	 * being rendered than can be displayed in the texture.
	 *
	 * @param	Width	[in]	New texture width.
	 * @param	Height	[in]	New texture height.
	 *
	 * @return	bool			Returns true on success, false on failure. 
	 */
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool ResizeTexture(int32 Width, int32 Height);


	/**
	 * Initializes the component with a number of lines with the same length. Existing data will be cleared. The data with which to initialize the texture needs to be already in a consistent format and
	 * will be moved (not copied) directly. This is more performant than adding the lines one by one, especially if lines are made up of very few segments.
	 * Color and width is only set per line, not per segment for now. if the Colors and Widths arrays are of size 1, the respective value will be used for all lines.
	 *
	 * It is recommended to call ReserveMemory before!
	 *
	 * @param	NumberOfLines			[in]	The number of total lines to add.
	 * @param	NumberOfSegmentsPerLine	[in]	The number of segments each line consists of.
	 * @param	Points					[in]	The points with which to initialize the texture. The TArray will be MOVED, so it is empty on return. 
	 * @param	Colors					[in]	The colors of the lines. If it has size 1, this color will be used for all lines.
	 * @param	Widths					[in]	The widths of the lines. If it has size 1, this width will be used for all lines.
	 *
	 * @return	int32			Id of the line, can be used to access it later on.
	 */
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	void InitializeLinesInBulk(int32 NumberOfLines, int32 NumberOfSegmentsPerLine, TArray<FVector4>& Points, const TArray<FLinearColor>& Colors, const TArray<float>& Widths);

	void InitializeLinesInBulkTesselate(int32 NumberOfLines, int32 NumberOfSegmentsPerLine, TArray<FVector4>& Points, const TArray<FLinearColor>& Colors, const TArray<float>& Widths);

	
	/**
	 * Adds a line and returns the respective ID of the line, which can be used to identify it for updating, modifying and removing.
	 * 
	 * @param	Line	[in]	The points of the line in consecutive order. 
	 * @param	Color	[in]	The color of the line.
	 * @param	Width	[in]	The width of the line.
	 *
	 * @return	int32			Id of the line, can be used to access it later on.
	 */
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	int32 AddLine(const TArray<FVector>& Line, FLinearColor Color, float Width = 1.0);


	int32 AddLine(TArray<FVector4>& Line, FLinearColor Color, float Width = 1.0, bool bMarkRenderStateDirty = true);
	
	/**
	 * Adds a line and returns the respective ID of the line, which can be used to identify it for updating, modifying and removing.
	 *
	 * @param	LineData	[in]	The Line data stored in a FEditorLineData struct.
	 *
	 * @return	int32				Id of the line, can be used to access it later on.
	*/
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	int32 AddLineFromEditorData(FEditorLineData& LineData);

	/**
	 * Adds a point at the end of the specified line.
	 *
	 * @param	LineId			[in]	The id of the line the points will be added to.
	 * @param	Point			[in]	The point to add.
	 *
	 * @return	bool				Returns true on successful addition of points.
	 */
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool AddPoint(int32 LineId, const FVector& Point);

	// Internal insertion function not callable via blueprints.
	bool InsertPoint(int32 LineId, int32 SegmentId, const FVector& Point, const FLinearColor& Color);

	/**
	 * Inserts a point into the specified line before the specified segment with the same color as the original line.
	 *
	 * @param	LineId			[in]	The id of the line the point will be inserted into.
	 * @param	SegmentId		[in]	The index of the segment.
	 * @param	Point			[in]	The point to insert.
	 *
	 * @return	bool				Returns true on successful insertion of points.
	 */
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool InsertPointWithSameColor(int32 LineId, int32 SegmentId, const FVector& Point);

	/**
	 * Inserts a point into the specified line before the specified segment with a specified color.
	 *
	 * @param	LineId			[in]	The id of the line the point will be inserted into.
	 * @param	SegmentId		[in]	The index of the segment.
	 * @param	Point			[in]	The point to insert.
	 * @param	Color			[in]	The color of the segment that comes after the new point, connecting it to the next point.
	 *
	 * @return	bool				Returns true on successful insertion of points.
	 */
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool InsertPointWithColor(int32 LineId, int32 SegmentId, const FVector& Point, const FLinearColor& Color);

	/**
	 * Adds a number of consecutive points at the end of the specified line.
	 *
	 * @param	LineId		[in]	The id of the line the points will be added to.
	 * @param	Points		[in]	The array of points that will be appended at the end of the line.
	 *
	 * @return	bool				Returns true on successful insertion of points.
	 */
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool AddPoints(int32 LineId, const TArray<FVector>& Points);

	/**
	 * Inserts a number of consecutive points into the specified line before the specified segment.
	 *
	 * @param	LineId		[in]	The id of the line the points will be inserted into.
	 * @param	SegmentId	[in]	The index of the segment before which the new points will be inserted. An index of 0 inserts the points at the very front
	 *								of the line, an index of Line.Num() just adds them to the end.
	 * @param	Points		[in]	The array of points that will be inserted.
	 * 
	 * @return	bool				Returns true on successful insertion.
	 */
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool InsertPoints(int32 LineId, int32 SegmentId, const TArray<FVector>& Points);

	// Update Functions

	/**
	 * This function updates the specified line without changing its connectivity/topology.
	 *
	 * @param	LineId	[in]		The id of the line which is updated.
	 * @param	Points	[in]		Updated linear array of points in the line, needs to be the same amount as the ones already existing.
	 *								The array will be MOVED, so Points will be empty on return.
	 * @return	bool				Returns true on successful update.
	 */	
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool UpdateLine(int32 LineId, TArray<FVector>& Points);


	/**
	 * This function updates the linear line data directly without changing connectivity or topology.
	 * It is the fastest way to update the data due to only executing a memcopy, but *requires* the Points array
	 * to be in the correct format.
	 *
	 * @param	LineIdStart	[in]	The index into the linear data array at which position to copy the data to.
	 * @param	Points		[in]	The array of points to be copied into the linear data array (and the texture respectively).
	 *								No formatting is done, so the w value of the points should already be correctly set, if needed.
	 *								The array will be MOVED, so Points will be empty on return.
	 * @return	bool				Returns true on successful update.
	 */
	bool UpdateLinesDataDirectly(int32 LineIdStart, TArray<FVector4>& Points);

	/**
	 * Updates a line from a FEditorLineData struct and returns true on success.
	 *
	 * @param	LineData	[in]	The Line data stored in a FEditorLineData struct.
	 *
	 * @return	bool				Returns true on successful update.
	*/
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool UpdateLineFromEditorData(const FEditorLineData& LineData);

	/**
	 * Fast function to draw the specified lines directly WITHOUT UPDATING THE INTERNAL LINE STATE.
	 * This is the fastest way to draw lines, as no memory needs to be copied, but the internal linear line data array will not be updated,
	 * possibly leading to data inconsitency. Only the texture used to render the points is updated. 
	 * 
	 *
	 * @param	LineIdStart	[in]	The index into the line data texture at which to start directly updating the points.
	 * @param	Points		[in]	The array of points to be used for the texture update.
	 *								No formatting is done, so the w value of the points should already be correctly set, if needed.
	 *								The array will be MOVED, not copied, so Points will be empty on return.
	 * @return	bool				Returns true on successful update.
	 */
	bool DrawLinesDirectly(int32 LineIdStart, TArray<FVector4>& Points);


	bool DrawLinesDirectly(int32 LineIdStart, const TArray<FVector4>& Points);
	
	bool DrawLinesDirectly(int32 LineIdStart, TArray<FVector4>* Points);
	
	bool DrawLinesDirectly(int32 LineIdStart, uint8* SrcData, int32 Num);
	
	/**
	 * This function updates the location of a given point in an existing line.
	 *
	 * @param	LineId	[in]	The id of the line.
	 * @param	PointId	[in]	The index of the point that is updated.
	 * @param	Point	[in]	The new position of the point.
	 * @return	bool			Returns true on successful update.
	 */
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool UpdatePoint(int32 LineId, int32 PointId, const FVector& Point);

	// todo
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool UpdatePoints(int32 LineId, int32 StartingPointId, TArray<FVector>& Points);

	// Removal Functions
	// todo
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool RemoveLine(int32 LineId);

	
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool RemoveLineFromEditorData(int32 LineId);


	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool RemoveAllEditorLines();
	
	// todo
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool RemovePoint(int32 LineId, int32 PointId);
	
	// todo
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool RemovePoints(int32 LineId, int32 StartingPointId, int32 NumberOfPoints);


	/**
	 * This function sets the color of a whole line.
	 *
	 * @param	LineId	[in]	The id of the line.
	 * @param	Color	[in]	The new color.
	 * 
	 * @return	bool			Returns true on successful update.
	 */	
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool SetLineColor(int32 LineId, const FLinearColor& Color);

	/**
	 * This function sets the color of a single segment in a line.
	 *
	 * @param	LineId		[in]	The id of the line.
	 * @param	SegmentId	[in]	The index of the segment.
	 * @param	Color		[in]	The new color.
	 *
	 * @return	bool			Returns true on successful update.
	 */
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool SetSegmentColor(int32 LineId, int32 SegmentId, const FLinearColor& Color);

	/**
	 * This function sets the width of a whole line.
	 *
	 * @param	LineId	[in]	The id of the line.
	 * @param	Width	[in]	The new width.
	 *
	 * @return	bool			Returns true on successful update.
	 */
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool SetLineWidth(int32 LineId, float Width);

	/**
	 * This function sets the width of a single segment in a line.
	 *
	 * @param	LineId		[in]	The id of the line.
	 * @param	SegmentId	[in]	The index of the segment.
	 * @param	Width		[in]	The new width.
	 *
	 * @return	bool				Returns true on successful update.
	 */
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool SetSegmentWidth(int32 LineId, int32 SegmentId, float Width);

	
	// todo
	UFUNCTION(BlueprintCallable, Category = "Components|InstancedLineComponent")
	bool ClearAllLines()
	{
		return false;
	}

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Components|InstancedLineComponent")
	int32 GetNumberOfLines() const
	{
		return LineMap.Num();
	}


	/**
	 * Returns the number of points (not segments) of a given line.
	 *
	 * @param LineId	[in]	Id of the line
	 *	
	 * @return int32			Number of points.	
	 */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Components|InstancedLineComponent")
	int32 GetNumberOfPointsInLine(int32 LineId) const
	{
		return LineMap[LineId].IndexArray.Num();
	}

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float SegmentLengthScale = 100;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	UTexture2D* PositionTexture;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	int32 TextureWidth;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	int32 TextureHeight;

	UPROPERTY(Transient)
	UMaterialInstanceDynamic* DynamicLineMaterial;

	UPROPERTY(EditAnywhere, DisplayName = "Lines", meta = (MakeEditWidget = true, EditFixedOrder))
	TArray<FEditorLineData> EditorLines;

	UPROPERTY(EditAnywhere)
	UMaterialInterface* LineMaterialInterface;
	
	UPROPERTY()
	TArray<FGPULineArray> LineMap;
	
//private:
	
	UPROPERTY()
	TArray<FVector4> LinearLineData;

	// Array that keeps track of editor lines. This is ONLY needed because the array can be cleared by a simple stupid button press in the
	// details panel, circumventing the PostEditChangeChainProperty function by clearing the array first. The change callback gets called, but
	// as the EditorLines array has already been cleared, it's not possible to backtrack the removed lines.
	UPROPERTY()
	TArray<int32> EditorLineIds;

	//UPROPERTY()
	//int32 NextFreeId;

	UPROPERTY()
	int32 CurrentTextureIndex;

	UPROPERTY()
	FIntPoint CurrentTextureMarker;

	UPROPERTY(Transient)
	bool bIsInitialized = false;

	private:
	// hacky way to initialize components.
	uint32 TickCount = 0;
	uint32 MaxTicksUntilInit = 5;
};


/** Helper class used to preserve texture pointer across blueprint reinstancing */
//USTRUCT()
//struct FUGPUInstancedLineComponentInstanceData : public FSceneComponentInstanceData
//{
//	GENERATED_BODY()
//public:
//	FUGPUInstancedLineComponentInstanceData() = default;
//	FUGPUInstancedLineComponentInstanceData(const UGPUInstancedLineComponent* InComponent)
//		: FSceneComponentInstanceData(InComponent)
//		, PositionTexture(InComponent->PositionTexture), LineMap(InComponent->LineMap)
//	{}
//	virtual ~FUGPUInstancedLineComponentInstanceData() = default;
//
//	virtual bool ContainsData() const override
//	{
//		return true;
//	}
//
//	virtual void ApplyToComponent(UActorComponent* Component, const ECacheApplyPhase CacheApplyPhase) override
//	{
//		Super::ApplyToComponent(Component, CacheApplyPhase);
//		CastChecked<UGPUInstancedLineComponent>(Component)->ApplyComponentInstanceData(this);
//	}
//
//	virtual void AddReferencedObjects(FReferenceCollector& Collector) override
//	{
//		Super::AddReferencedObjects(Collector);
//		Collector.AddReferencedObject(PositionTexture);
//	}
//
//public:
//
//	
//	
//	//UPROPERTY()
//	UTexture2D* PositionTexture;
//	
//	//UPROPERTY()	
//	UMaterialInstanceDynamic* DynamicMaterial;
//	
//	//UPROPERTY()
//	TMap<int32, FGPULineArray> LineMap;
//};